FROM golang:1.16

RUN mkdir /app
WORKDIR /app
COPY ./go.mod /app
COPY ./go.sum /app
RUN go mod download
COPY ./ /app
RUN go build
RUN mv /app/pig /pig
RUN mv /app/resources /resources
RUN rm -rf /app/*
RUN mv /pig /app/pig
RUN mv /resources /app/resources
EXPOSE 8000
CMD /app/pig
